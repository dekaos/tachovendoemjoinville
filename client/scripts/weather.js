var weatherDataNowDetails;
var dateString;
var OpenWeatherMap;
var mapIcon;
var width  = 575;
var height = 400;
var left   = ($(window).width()  - width)  / 2;
var top    = ($(window).height() - height) / 2;
var opts   = 'status=1' +
    ',width='  + width  +
    ',height=' + height +
    ',top='    + top    +
    ',left='   + left;

var accentsTidy = function(s) {
    var i;
    var r = s.toLowerCase();
    r = r.replace(new RegExp('[àáâãäå]', 'g'),'a');
    r = r.replace(new RegExp('æ', 'g'),'ae');
    r = r.replace(new RegExp('ç', 'g'),'c');
    r = r.replace(new RegExp('[èéêë]', 'g'),'e');
    r = r.replace(new RegExp('[ìíîï]', 'g'),'i');
    r = r.replace(new RegExp('ñ', 'g'),'n');
    r = r.replace(new RegExp('[òóôõö]', 'g'),'o');
    r = r.replace(new RegExp('œ', 'g'),'oe');
    r = r.replace(new RegExp('[ùúûü]', 'g'),'u');
    r = r.replace(new RegExp('[ýÿ]', 'g'),'y');
    return r;
};

Template.weather.helpers({
    weatherDataNow: function () {
        weatherDataNowDetails = WeatherNow.findOne();
        dateString = moment(weatherDataNowDetails.weather.submitted).format("DD.MM.YYYY HH:mm");
        weatherDataNowDetails.weather.submitted = dateString;
        mapIcon = weatherDataNowDetails.weather.icon;
        return weatherDataNowDetails;
    }
});

Template.search.helpers({
    searchResult: function () {
        return Session.get('result');
    }
});

Template.weather.events({
    'click .about-link': function (e) {
        e.preventDefault();
        $(e.target).find('span').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
        $('.about-txt').slideToggle();
    },
    'click .close': function (e) {
        $('.search-result').removeClass('open');
        $('#city').val('');
        Session.set('city-name', null);
    },
    'click .twitter': function (e) {
        e.preventDefault();
        var url = $('.twitter').attr('href');
        window.open(url, 'twitter', opts);
    },
    'click .facebook': function (e) {
        e.preventDefault();
        var url = 'https://www.facebook.com/dialog/feed?app_id=263784757166119&display=popup&picture=http://tachovendoemjoinville-ironrocks.rhcloud.com/images/weather.png&link=http://tachovendoemjoinville-ironrocks.rhcloud.com/&redirect_uri=http://tachovendoemjoinville-ironrocks.rhcloud.com/close';
        window.open(url, 'sharer', opts);
    },
    'click .gplus': function (e) {
        e.preventDefault();
        var url = 'https://plus.google.com/share?url=http://tachovendoemjoinville-ironrocks.rhcloud.com/';
        window.open(url, 'gplus', opts);
    },
    'submit #search-city': function (e) {
        e.preventDefault();
        var cityName   = accentsTidy($(e.target).find('#city').val());
        var weatherUrl = 'http://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&type=like&units=metric&lang=pt&APPID=50cee3eb274c3567972054e2c538a34b';

        if (Session.get('city-name') !== cityName) {
            HTTP.call('POST', weatherUrl, function (error, result) {

                if (!error) {
                    if (result.data.cod == 404) {
                        var input = $('#city').val();
                        $('#city').val('').attr('placeholder', 'Nenhum resultado para: ' + input);
                    } else if (result.data.cod == 200) {

                        $('#mini-map').html('');
                        $('.search-result').addClass('open');
                        $('#city').attr('placeholder', 'Procurar cidade');

                        Meteor.call('getMsgByCod', result.data.weather[0].id, function (error, result) {
                            Session.set('desc', null);
                            if (!error) {
                                Session.set('desc', result);

                                return searchedData();
                            }
                        });

                        function searchedData() {
                            if (cityName !== accentsTidy(result.data.name)) {
                                var approximate = '* O resultado mais próximo para ' + cityName + ' é ' + result.data.name;
                                Session.set('approximate', approximate);
                            } else {
                                Session.set('approximate', '');
                            }

                            Session.set('result', {
                                appr: Session.get('approximate'),
                                name: result.data.name,
                                temp: result.data.main.temp,
                                flag: 'http://openweathermap.org/images/flags/' + result.data.sys.country.toLowerCase() + '.png',
                                desc: Session.get('desc'),
                                pres: result.data.main.pressure,
                                humi: result.data.main.humidity,
                                wind: result.data.wind.speed
                            });

                            var nicon = 'http://openweathermap.org/img/w/' + result.data.weather[0].icon + '.png';

                            return OpenWeatherMap(result.data.coord.lat, result.data.coord.lon, nicon, 'mini-map');
                        }
                    } else {
                        alert('Erro desconhecido ao procurar cidade.');
                    }
                } else {
                    console.log(!error);
                }
            });

            Session.set('city-name', cityName);
        }
    }
});

Template.weather.rendered = function () {
    Session.set('image', '');
    setTimeout(function() {
        $('#main, #footer-wrapper').animate({'opacity': 1});
    }, 300);

    $.getScript("//openlayers.org/api/OpenLayers.js", function () {
        $.getScript("//openweathermap.org/js/OWM.OpenLayers.1.3.4.js", function () {
            var nicon = mapIcon;
            return OpenWeatherMap(-26.3044084, -48.8463832, nicon, 'location-map');
        });
    });

    html2canvas(document.getElementById('weather-now'), {
        onrendered: function(canvas) {
            var data = canvas.toDataURL('image/png');
            Session.set('image', data);
        }
    });
};

OpenWeatherMap = function (latitude, longitude, nicon, container) {
    var lat = latitude;
    var lon = longitude;
    var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
    var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
    var position       = new OpenLayers.LonLat(lon, lat).transform( fromProjection, toProjection);
    var map     = new OpenLayers.Map(container);
    var mapnik  = new OpenLayers.Layer.OSM();
    var markers = new OpenLayers.Layer.Markers( "Markers" );

    map.addLayer(markers);

    var size   = new OpenLayers.Size(50, 50);
    var offset = new OpenLayers.Pixel(-(size.w/1.2), -size.h/1.1);
    var icon   = new OpenLayers.Icon(nicon, size, offset);
    markers.addMarker(new OpenLayers.Marker(position, icon));
    markers.addMarker(new OpenLayers.Marker(position, icon.clone()));

    map.addLayers([mapnik]);
    map.setCenter(position, 13 );
};

