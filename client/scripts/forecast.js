/**
 * Created by dev-rocks on 7/18/15.
 */
var forecastData;
var chartLabel        = [];
var chartDataTemp     = [];
var chartDataWind     = [];
var chartDataPressure = [];

Template.chart.rendered = function () {
    function getForecast() {
        forecastData = WeatherForecast.findOne();
        for (var i in forecastData.forecast.data.list) {
            if (i <= 7) {
                chartLabel.push(moment(forecastData.forecast.data.list[i].dt_txt).format('DD MMM. HH:mm') + ' ' + forecastData.forecast.data.list[i].weather[0].description);
                chartDataTemp.push(forecastData.forecast.data.list[i].main.temp);
                chartDataWind.push(forecastData.forecast.data.list[i].wind.speed);
                chartDataPressure.push(forecastData.forecast.data.list[i].main.pressure);
            }
        }
        return renderChart();
    }

    function renderChart() {
        $('#forecast').highcharts({
            credits: {
                enabled: false
            },
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Previsão para as próximas horas'
            },
            xAxis: [{
                categories: chartLabel,
                crosshair: true
            }],
            yAxis: [{
                labels: {
                    format: '{value}°C',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Temperatura',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Vento',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} m/s',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }, { // Tertiary yAxis
                title: {
                    text: 'Pressão',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                   }
                },
                labels: {
                    format: '{value} hpa',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                   }
                },
                opposite: true
            }],
            tooltip: {
                useHTML: true,
                shared: true
                //formatter: function () {

                //}
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 65,
                verticalAlign: 'top',
                y: 15,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Vento',
                type: 'spline',
                yAxis: 1,
                data: chartDataWind,
                tooltip: {
                    valueSuffix: ' m/s'
                }
            },
            {
                name: 'Temperatura',
                type: 'spline',
                data: chartDataTemp,
                tooltip: {
                    valueSuffix: ' °C'
                }
            },
            {
                name: 'Pressão',
                type: 'spline',
                yAxis: 2,
                data: chartDataPressure,
                tooltip: {
                    valueSuffix: ' hpa'
                }
            }]
        });
    }

    return getForecast();
};

