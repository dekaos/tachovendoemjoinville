Router.configure({
    loadingTemplate: 'loading',
    layoutTemplate: 'layout',
    trackPageView: true
});

Router.map(function () {
    this.route('weather', {
        path: '/',
        trackPageView: true,
        waitOn: function () {
            return [
                Meteor.subscribe('weatherforecast'), Meteor.subscribe('weathernow')
            ]
        }
    });
    this.route('close', function () {
        return window.close();
    })
});

Router.onBeforeAction('loading');