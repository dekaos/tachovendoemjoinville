Meteor.publish('weathernow', function () {
    return WeatherNow.find({}, {sort: {$natural: -1}, limit: 1});
});

Meteor.publish('weatherforecast', function () {
    return WeatherForecast.find({}, {sort: {$natural: -1}, limit: 1});
});