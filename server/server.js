Meteor.startup(function () {
    var urlWeatherNow = 'http://api.openweathermap.org/data/2.5/weather?id=3459712&units=metric&type=accurate&lang=pt&APPID=50cee3eb274c3567972054e2c538a34b';
    var urlForecast   = 'http://api.openweathermap.org/data/2.5/forecast?id=3459712&units=metric&type=accurate&lang=pt&APPID=50cee3eb274c3567972054e2c538a34b';

    function getWeatherNow () {
        HTTP.call('POST', urlWeatherNow, function (error, result) {
            if (!error) {
                storeWeatherNowData(result);
            }
        });
    }

    function getWeatherForecast () {
        HTTP.call('POST', urlForecast, function (error, result) {
            if (!error) {
                for (var i in result.data.list) {
                    result.data.list[i].weather[0].description = codWeather(result.data.list[i].weather[0].id);
                }
                storeWeatherForecast(result);
            }
        });
    }

    function codWeather (cod) {
        var codeMsg = WeatherCodes.findOne();
        return codeMsg.code[cod];
    }

    function storeWeatherForecast(result) {
        if (result.statusCode === 200) {
            WeatherForecast.insert({
                forecast: result
            })
        }
    }

    function storeWeatherNowData(result) {
        if (result.statusCode === 200) {
            var fullDate = new Date();
            WeatherNow.insert({
                weather: {
                    submitted: fullDate,
                    city_id: result.data.id,
                    city_name: result.data.name,
                    condition: result.data.weather[0].main,
                    condition_datetime: result.data.dt,
                    description: codWeather(result.data.weather[0].id),
                    temp: result.data.main.temp,
                    temp_max: result.data.main.temp_max,
                    temp_min: result.data.main.temp_min,
                    pressure: result.data.main.pressure,
                    humidity: result.data.main.humidity,
                    wind_speed: result.data.wind.speed,
                    wind_deg: result.data.wind.deg,
                    longitude: result.data.coord.lon,
                    latitude: result.data.coord.lat,
                    sunrise: result.data.sys.sunrise,
                    sunset: result.data.sys.sunset,
                    icon: 'http://openweathermap.org/img/w/' + result.data.weather[0].icon + '.png',
                    cod: result.data.cod
                }
            });
        }
        getWeatherForecast();
    }
    getWeatherNow();
    if (WeatherNow.find().count() === 0) {
        getWeatherNow();
    }

    if (WeatherForecast.find().count() === 0) {
        getWeatherForecast();
    }

    Meteor.setInterval(function () {
        getWeatherNow();
    }, 20*60*1000);

    Meteor.methods({
        getMsgByCod: function (cod) {
            return codWeather(cod);
        }
    });
});